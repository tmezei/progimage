package io.cruftyloop.progimage.web

import io.cruftyloop.progimage.core.StorageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import reactor.test.StepVerifier
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class EndpointsSpec extends Specification {

  @Autowired
  WebTestClient webTestClient

  @Autowired
  StorageService storageService

  @Autowired
  Map filesystem

  @Autowired
  TestRestTemplate testRestTemplate

  def "returns files by id"() {
    given:
    filesystem.put(id, content)

    when:
    WebTestClient.ResponseSpec response = webTestClient.get()
                                                       .uri("/{id}.png", ["id": id])
                                                       .exchange()

    then:
    def res = response.expectStatus().isOk()
                      .expectHeader().contentType("image/png")
                      .returnResult(String)

    res.responseBodyContent == content.bytes

    where:
    id = "sample"
    content = new MockMultipartFile("sample", new ClassPathResource("sample.png").getInputStream().bytes)
  }

  def "returns HTTP 404 NOT_FOUND when asked for a non-existent resource"() {
    given: "the storage service has nothing in it"
    filesystem.clear()

    when:
    WebTestClient.ResponseSpec response = webTestClient.get()
                                                       .uri("/nonexistent.jpg")
                                                       .exchange()

    then:
    response.expectStatus().isNotFound()
  }

  @Ignore
  def "allows upload"() {
    given: "the storage service has nothing in it"
    filesystem.clear()

    when:
    MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>()
    map.add("file", resource)

    def response = testRestTemplate.postForEntity("/", map, String)

    then:
    response.getStatusCode().is2xxSuccessful()

    StepVerifier.create(storageService.findById(response.getBody())).expectNext(resource).expectComplete()

    where:
    resource = new MockMultipartFile("sample", new ClassPathResource("sample.png").getInputStream().bytes)

  }


}
