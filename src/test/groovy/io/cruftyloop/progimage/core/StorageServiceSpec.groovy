package io.cruftyloop.progimage.core

import org.springframework.mock.web.MockMultipartFile
import reactor.core.publisher.Flux
import reactor.test.StepVerifier
import spock.lang.Specification

class StorageServiceSpec extends Specification {

  def "allows file upload"() {
    given: "the storage service is running"
    def seq = Mock(Sequencer)
    def storage = new StorageService(seq, [:])

    when: "I upload a file"
    def response = storage.upload(file)

    then: "it returns an id for a file"
    StepVerifier.create(response)
                .expectNext(id)
                .expectComplete()

    1 * seq.next() >> id

    where:
    file = new MockMultipartFile("foo.jpg", "foo.jpg".bytes)
    id = "id-1"
  }

  def "allows retrieving the file by id"() {
    given: "I stored two files in the storage"
    def storage = new StorageService(Mock(Sequencer), files)

    when: "I find a file by id"
    def response = storage.findById(id)

    then: "it returns the file stored"
    StepVerifier.create(response)
                .expectNext(contents)
                .expectComplete()


    when: "I search for a file that's not in the storage"
    response = storage.findById(notstored)

    then: "I get an empty response"
    StepVerifier.create(response)
                .expectComplete()

    where:
    id = "id0"
    contents = "id0".bytes
    files = [id: contents, "id1": "id1".bytes, "id2": "id2".bytes]
    notstored = "notstored"
  }

}
