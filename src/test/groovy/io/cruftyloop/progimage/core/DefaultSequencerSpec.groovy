package io.cruftyloop.progimage.core

import spock.lang.Specification

class DefaultSequencerSpec extends Specification {

  def "returns ids"() {
    given:
    def sequencer = new DefaultSequencer(0)

    expect:
    sequencer.next() == "1"

    and:
    sequencer.next() == "2"
  }
}
