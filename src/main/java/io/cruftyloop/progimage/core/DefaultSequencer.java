package io.cruftyloop.progimage.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

@Service
public class DefaultSequencer implements Sequencer {

  private final AtomicLong counter;

  public DefaultSequencer(@Value("10000000") long initialValue) {
    this.counter = new AtomicLong(initialValue);
  }

  @Override
  public String next() {
    return "" + counter.incrementAndGet();
  }
}
