package io.cruftyloop.progimage.core;

import reactor.core.publisher.Flux;

public interface Sequencer {

  String next();
}
