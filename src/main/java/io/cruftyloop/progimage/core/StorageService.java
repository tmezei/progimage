package io.cruftyloop.progimage.core;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
public class StorageService {

  private final Sequencer sequencer;
  private final Map<String, MultipartFile> files;

  public StorageService(Sequencer sequencer, Map<String, MultipartFile> files) {
    this.sequencer = sequencer;
    this.files = files;
  }

  public Mono<String> upload(MultipartFile file) {
    String id = sequencer.next();
    files.put(id, file);

    return Mono.just(id);
  }

  public Mono<MultipartFile> findById(String id) {
    return Mono.justOrEmpty(files.get(id));
  }
}
