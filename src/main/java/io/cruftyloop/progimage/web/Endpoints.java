package io.cruftyloop.progimage.web;

import io.cruftyloop.progimage.core.StorageService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.IOException;

@RestController
public class Endpoints {

  private final StorageService storageService;

  public Endpoints(StorageService storageService) {
    this.storageService = storageService;
  }


  @GetMapping("/{filename:[a-zA-Z0-9]+}.{extension:[a-zA-Z0-9]+}")
  public Mono<ResponseEntity<ByteArrayResource>> serve(@PathVariable("filename") String filename,
                                                       @PathVariable("extension") String extension) {
    return storageService.findById(filename)
                         .flatMap(mf -> {
                           try {
                             return Mono.just(new ByteArrayResource(mf.getBytes()));
                           } catch (IOException e) {
                             return Mono.error(e);
                           }
                         })
                         .map(r -> ResponseEntity.ok()
                                                 .header(HttpHeaders.CONTENT_DISPOSITION,
                                                         "attachment; filename=\"" + filename + "." + extension + "\"")
                                                 .header(HttpHeaders.CONTENT_TYPE, "image/" + extension)
                                                 .body(r))
                         .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @PostMapping("/")
  public Mono<String> upload(@RequestParam("file") MultipartFile file) {
    return storageService.upload(file);
  }

}
